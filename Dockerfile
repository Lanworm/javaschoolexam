FROM ubuntu:zesty
FROM maven:3
WORKDIR /app
COPY pom.xml ./
RUN mvn install clean --fail-never -B -DfailIfNoTests=false
COPY . ./