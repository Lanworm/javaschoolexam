package com.tsystems.javaschool.tasks.calculator;

import java.util.*;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     * parentheses, operations signs '+', '-', '*', '/'<br>
     * Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */

    private static final Map<String, Integer> operations;

    static {
        operations = new HashMap<>();
        operations.put("*", 1);
        operations.put("/", 1);
        operations.put("+", 2);
        operations.put("-", 2);
    }

    public String evaluate(String statement) {
        if (statement == null) {
            return null;
        }
        String clearedStatement = statement.replaceAll(" ", "");
        char[] statementCharArr = clearedStatement.toCharArray();
        List<String> statementList = new ArrayList<>();

        String s = "";
        for (char sChar : statementCharArr) {
            if (!operations.containsKey(String.valueOf(sChar)) && !String.valueOf(sChar).equals(")") && !String.valueOf(sChar).equals("(")) {
                s = s + sChar;
            } else {
                if (s.length() > 0) {
                    statementList.add(s);
                }
                s = "";
                statementList.add(String.valueOf(sChar));
            }
        }
        if (s.length() > 0) {
            statementList.add(s);
        }


        try {
            double calculatedResult = Double.parseDouble(calculate(statementList).get(0));
            String result;
            boolean whole_number = (calculatedResult % 1) == 0;
            if (whole_number) {
                result = String.valueOf(Math.round(calculatedResult));
            } else {
                result = String.valueOf(calculatedResult);
            }
            return result;
        } catch (Exception e) {
            return null;
        }

    }

    private List<String> calculate(List<String> expression) {

        HashMap<Integer, int[]> brackets = new HashMap<>();
        int lIndex = 0;
        int RIndex = 0;
        for (int i = 0; i < expression.size(); i++) {
            boolean hasLBracket = expression.get(i).equals("(");
            boolean hasRBracket = expression.get(i).equals(")");
            if (hasLBracket) {
                int[] arr;
                if (brackets.containsKey(lIndex)) {
                    arr = brackets.get(lIndex);
                } else {
                    arr = new int[2];
                }
                arr[0] = i;
                brackets.put(lIndex, arr);
                lIndex++;
            }
            if (hasRBracket) {
                RIndex++;
                int[] arr;
                if (brackets.containsKey(brackets.size() - RIndex)) {
                    arr = brackets.get(brackets.size() - RIndex);
                } else {
                    arr = new int[2];
                }
                arr[1] = i;
                brackets.put(brackets.size() - RIndex, arr);
            }
            if (lIndex == RIndex) {
                RIndex = 0;
            }
        }

        for (int b : brackets.keySet()) {
            int[] bracket;
            bracket = brackets.get(b);
            expression.remove(bracket[0]);
            expression.remove(bracket[1]-1);
            List<String> subResult = calculate(expression.subList(bracket[0] , bracket[1]-1));
            List<String> result = calculate(expression);
            break;
        }

        HashMap<Integer, String> priority = new HashMap<>();
        for (String o : operations.keySet()) {
            int i = expression.indexOf(o);
            if (i != -1) {
                int index = Integer.parseInt(operations.get(o) + String.valueOf(i));
                priority.put(index, o);
            }
        }
        Map<Integer, String> sortedPriority = new TreeMap<>(
                Comparator.naturalOrder()
        );
        sortedPriority.putAll(priority);

        for (int ok : sortedPriority.keySet()) {
            String o = sortedPriority.get(ok);
            int i = expression.indexOf(o);
            Double a = Double.parseDouble(expression.get(i - 1));
            Double b = Double.parseDouble(expression.get(i + 1));
            if (o.equals("/") && b == 0) throw new IllegalArgumentException("division by zero");
            double result = 0;
            switch (o) {
                case "/":
                    result = a / b;
                    break;
                case "*":
                    result = a * b;
                    break;
                case "-":
                    result = a - b;
                    break;
                case "+":
                    result = a + b;
                    break;
            }
            expression.add(i - 1, String.valueOf(result));
            expression.subList(i, i + 3).clear();
        }

        return expression;
    }
}
