package com.tsystems.javaschool.tasks.pyramid;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        if (!isTriangular(inputNumbers.size())) throw new CannotBuildPyramidException("pyramid cannot be build with given input");
        if (inputNumbers.contains(null)) throw new CannotBuildPyramidException("pyramid cannot be build with given input");
        int rows = Math.toIntExact(Math.round((-1 + Math.sqrt(1 + 8 * inputNumbers.size())) / 2));
        int cols = ((rows - 1) * 2) + 1;
        inputNumbers.sort(Comparator.naturalOrder());
        int[][] result = new int[rows][cols];

        for (int[] row : result) {
            Arrays.fill(row, 0);
        }

        int center = (cols / 2);
        int numbersInRow = 1;
        int inputNumbersIndex = 0;

        for (int i = 0, offset = 0; i < rows; i++, offset++, numbersInRow++) {
            int start = center - offset;
            for (int j = 0; j < numbersInRow * 2; j += 2, inputNumbersIndex++) {
                result[i][start + j] = inputNumbers.get(inputNumbersIndex);
            }
        }
        return result;
    }

    private Boolean isSquare(int x) {
        return (Math.sqrt(x) == (int) Math.sqrt(x));
    }

    private Boolean isTriangular(int z) {
        return (isSquare(8 * z + 1));
    }

}
