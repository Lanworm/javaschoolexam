package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        if (x == null || y == null) throw new IllegalArgumentException("invalid args");
        if (x.size() == 0) {
            return true;
        }

        if (y.size() == 0) {
            return false;
        }

        if (String.valueOf(y.get(0).getClass()).equals("class java.lang.String")) {
            String string = y.toString().replaceAll("\\W", "");
            y = Stream.of(string.split(",")).collect(toList());
        }

        int result = 0;
        for (int i = 0; i < x.size(); i++) {
            int hasInd = y.indexOf(x.get(i));
            if (!(hasInd != -1 && hasInd >= i)) {
                result++;
            }
        }

        return result == 0;
    }
}
